import json

data = {
    "president": {
        "name": "Zaphod Beeblebrox",
        "species": "Betelgeusian"
    }
}

with open("data_file.json", "w") as write_file:
    json.dump(data, write_file)

## python -i test_for_fun.py
#json.dumps(data, indent=4)

blackjack_hand = (8, "Q")
encoded_hand = json.dumps(blackjack_hand)
decoded_hand = json.loads(encoded_hand)
 
print(blackjack_hand == decoded_hand) # False
 
print(type(blackjack_hand)) # <class 'tuple'>
print(type(decoded_hand)) # <class 'list'>
 
print(blackjack_hand == tuple(decoded_hand)) # True

print(blackjack_hand)
print(decoded_hand)